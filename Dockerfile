FROM node:lts

COPY . .
RUN npm install
RUN node


EXPOSE 3000

CMD [ "node", "server.js" ]